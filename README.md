# OpenML dataset: product-relevance

https://www.openml.org/d/43780

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The "goal" for this dataset is to predict how relevant each product are to each customer, to ensure that future recommendations and sales are relevant to the customer.
Content
This database contains 22 attributes, which are some of the most common attributes collected when a company sells a product to a customer.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43780) of an [OpenML dataset](https://www.openml.org/d/43780). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43780/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43780/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43780/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

